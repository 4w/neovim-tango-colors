# This colorscheme plugin was moved

The “Neovim Tango Colors” colorscheme plugin was moved away from GitLab. The new location can be found here:

* <https://git.0x7be.net/dirk/neovim-tango-colors>

Just update your plugin manager configuration.

```lua
-- Load the colorscheme using Packer
require('packer').startup(function(use)
    use({
        'https://git.0x7be.net/dirk/neovim-tango-colors',
        config = function()
            vim.opt.termguicolors = true
            vim.cmd.colorscheme('tango-colors')
        end
    })
end)
```

So basically all `https://gitlab.com/4w/neovim-tango-colors` become `https://git.0x7be.net/dirk/neovim-tango-colors`

The functionality of the colorscheme plugin did not change. It is still in active development and changes or fixes are made when needed. All that has changed is the public Git hosting location.
